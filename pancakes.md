# Pancakes

## Ingredients

- **3 cups** Plus 2 Tablespoons Cake Flour
- **3 tbsp** Baking **Powder**
- **2 tbsp** Sugar
- **1/2 tsp** Salt
- **2 cups** Milk
- **2 whole** Large Eggs
- **3 tsp** Vanilla
- **4 tbsp** Butter, Melted
- Extra Butter
- Maple Or Pancake Syrup

## Directions

Mix together dry ingredients in large bowl.

Mix together milk, eggs, and vanilla in a separate bowl.

Add wet ingredients to dry ingredients, stirring very gently until just combined.

Melt butter and add it to the batter, stirring gently to combine. Stir in more milk if needed for thinning.

Cook on a greased skillet over medium-low heat until golden brown. Serve with an obscene amount of butter and warm syrup.
